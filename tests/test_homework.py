import pytest
import random
import string

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage
from pages.project_page import ProjectPage
from pages.add_project_page import AddProjectPage
from pages.project_details_page import ProjectDetailsPage

from utils.random import RandomUnit


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")
    yield browser
    browser.quit()


def test_new_project(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    assert browser.find_element(By.CSS_SELECTOR, "content_title").text == "Projects"


    projects_page = ProjectPage(browser)
    projects_page.clik_add_project()
    assert browser.find_element(By.CSS_SELECTOR, "content_title").text == "New project"

    add_project_page = AddProjectPage(browser)
    project_prefix = RandomUnit.get_random_string(5)
    project_name = f"4_testers {project_prefix}"
    add_project_page.add_project(project_name, project_prefix)
    assert browser.find_element(By.CSS_SELECTOR, "j_info_box"). is displayed()

    project_details_page = ProjectDetailsPage(browser)
    project_details_page.close_new_project_confrimation()
    project_details_page.clik_cockpi()

    cockpit_page.click_administration()
    assert browser.find_element(By.CSS_SELECTOR, "content_title").text == "Projects"
